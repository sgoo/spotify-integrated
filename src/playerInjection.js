function init(){
	console.log("Init Spotify Integrated");
}



function getStatus(){
	spStat = {};
	
	spStat.bg = $('#cover-art .sp-image-img').css('background-image');
	spStat.track = $('#track-name').text();
	spStat.artist = $('#track-artist').text();
	spStat.playing = $('#play-pause').hasClass('playing')
	return spStat;
}

chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
	if(request.hasOwnProperty('playpause')){
		$('#play-pause').click();
	}
	if(request.hasOwnProperty('spStat')){
		sendResponse(getStatus());
	}
});








window.onload=init;

