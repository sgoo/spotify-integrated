var startTime = new Date().getTime();
var lastRequest = 0;
var lastReply = new Date().getTime();
var updateInterval = 1000;
var failState = null;
var isPlaying = null;

function getStatus(){
	// 0.9 because js timers are not very perfect
	// if(new Date.getTime() - startTime
	if(new Date().getTime() - startTime > updateInterval * 1.8){
		if(Math.abs(lastReply - lastRequest) > updateInterval * 1.2){ // 1.2 to save us from randomness
			console.log('setting fail');
			chrome.browserAction.setIcon({path: {19: "spotifyFail19.png",38: "spotifyFail38.png"}});
			failState = true;
		}
	}
	lastRequest = new Date().getTime();
 	sendTabMsg({spStat: true}, onStatus);
	setTimeout(getStatus, updateInterval);
}


function onStatus(spStat){
	if(spStat == undefined){
		return;
	}
	lastReply = new Date().getTime();

	if(isPlaying == null){
		// init it to the opposite to force the next if to be true
		isPlaying = !spStat.playing;
	}
	
	if(failState == null || failState){
		failState = false;
		console.log("setting non-fail");
		chrome.browserAction.setIcon({path: {19: "spotify19.png",38: "spotify38.png"}});
	}

	if(isPlaying != spStat.playing){
		isPlaying = spStat.playing;
		if(isPlaying){
			chrome.browserAction.setIcon({path: {19: "spotify19.png",38: "spotify38.png"}});
		}else{
			chrome.browserAction.setIcon({path: {19: "spotifyPause19.png",38: "spotifyPause38.png"}});
		}
	}

}

getStatus()