
function pp(){
	sendTabMsg({playpause: true,spStat: true}, onStatus);
}

function getStatus(){
	sendTabMsg({spStat: true}, onStatus);
	setTimeout(getStatus, updateInterval);
}

function onStatus(spStat){
	$('#image').css('background-image', spStat.bg);
	$('#artist h3').text(spStat.artist);
	$('#track h4').text(spStat.track);
}

$(function (){
	$('#image').click(pp);
});


getStatus();
